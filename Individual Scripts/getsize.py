#! /usr/bin/env python3

"""File to save useful functions."""

from pathlib import Path


def getsize(directory):
    """Returns the size of the directory in kilobytes."""
    Kilobyte = 1024
    Megabyte = 1024 ** 2
    size = sum(f.stat().st_size
               for f in directory.glob("**/*")
               if f.is_file())
    return size / Megabyte


def main():
    directory = input("Drag a directory into the window: ").strip("\" ")
    directory = Path(directory).resolve()

    print()
    for file in directory.iterdir():
        if file.is_dir():
            print(f"{file.name} Size: {getsize(file)} MB")
    input("\nComplete...")


if __name__ == "__main__":
    main()
