#! /usr/bin/env python3

# gen-harvest-csv.py
# Generates a CSV for upload to METRC
# Joseph Carpinelli
# June 21st, 2019


# Get a Veg_List
# Add getStrainCode
# Add new date format

# Move excel.py constants to templates
# consider moving templates to constants.etc

import sys
import os
import csv
import re
import datetime
from datetime import timedelta
import string

import openpyxl


""" UTILS FUNCTIONS """


def getDateFormat(Separator, Leading_Zero=True):
    """Gets a date format as a string.
    Avoids long line obfuscation.
    """
    if Leading_Zero:
        return Leading_Zero_Date_Format.format(Separator=Separator)
    else:
        return Date_Format.format(Separator=Separator)


def getDate(Separator='/'):
    """Get mm/dd/yyyy formatted date string."""
    # date_format = getDateFormat(Separator, Leading_Zero=False)
    date = datetime.datetime.now()

    todays_date = f"{date.month}{Separator}{date.day}{Separator}{date.year}"

    """
    # Remove Leading Zeros
    date.strftime(date_format).replace("X0", "X").replace("X", "")

    todays_date = str(date)
    """

    return todays_date


def getDateForLast(Weekday: str = "Monday", Separator='/') -> str:
    """Returns a string containing the date based on the value enter
    for 'Weekday'."""
    Days = {"Monday": 0, "Tuesday": 1, "Wednesday": 2, "Thursday": 3,
            "Friday": 4, "Saturday": 5, "Sunday": 6}
    if Weekday not in Days:
        raise KeyError(f"{Weekday} is not a known format for day of the week.")
    today = datetime.date.today()
    offset = (today.weekday() - Days[Weekday]) % 7
    last_date = today - timedelta(days=offset)
    return datetime.date.strftime(last_date,
                                  getDateFormat(Separator))  # Formatting


def getWednesdaysDate(Separator='/'):
    """Gets the previous Wednesdays date using getDateFormat for the format."""
    today = datetime.date.today()
    offset = (today.weekday() - 2) % 7
    last_wednesday = today - timedelta(days=offset)
    wednesday = datetime.date.strftime(last_wednesday,
                                       getDateFormat(Separator)) # Formatting

    return wednesday


def getWordFromNumber(number):
    """Gets a spelled string from an individual digit."""
    isNumeric = str(number).isnumeric()
    if not isNumeric:
        raise TypeError

    number = int(number)
    isOutOfRange = (number > 9 or number < 0)
    if isOutOfRange:
        raise IndexError

    return Spelled_Digits[number]


def getWordFromAbbreviation(abbreviation):
    """Gets a fully-spelled version of an abbreviation.
    If abbreviation is not in templates.Spelled_Words a LookupError is
    raised.
    """
    abbreviation = abbreviation.upper()
    if abbreviation not in Spelled_Words:
        raise LookupError

    return Spelled_Words[abbreviation]


def getSpelledStrain(Strain):
    """Gets a fully-spelled version of a strain with number and
    abbreviations.
    """
    spelled_strain = []

    for word in Strain.split(' '):
        word = word.strip(string.punctuation)
        if word.isnumeric():
            if int(word) in Spelled_Digits:
                spelled_strain.append(getWordFromNumber(word))

        elif word.upper() in Spelled_Words:
            spelled_strain.append(getWordFromAbbreviation(word))

        else:
            spelled_strain.append(word)

    return " ".join(spelled_strain)


def getStrainCode(Strain):
    """Gets the first two characters of each word from a fully spelled
    strain name."""
    strain_code = str()

    spelled_strain = getSpelledStrain(Strain)

    for word in spelled_strain.split(' '):
        strain_code += word[:2]

    return strain_code.upper()


def getStrainCodes(Strains):
    """Gets a list of strain codes from a list of strains."""
    strain_codes = []

    for strain in Strains:
        strain_codes.append(getStrainCode(strain))

    return strain_codes


def convertToStripped(in_sheetlist):
    """Converts in_sheetlist by applying .strip() to all values"""
    for row in in_sheetlist:
        for cell_value in row:
            if isinstance(cell_value, str):
                cell_value = cell_value.strip()


def convertNoneToString(in_sheetlist):
    """Converts in_sheetlist None values to empty string."""
    for row in in_sheetlist:
        for cell_value in row:
            if cell_value is None:
                cell_value = str()


def getRoomGroups(in_row):
    """Takes a row of text from getSheetlist.
    Returns a list containing the letter and number of the room.
    If match is not found,
    Returns a list containing an empty string and then a 0.
    """

    regex_match = (Room_Pattern
                   .search(str(in_row[Room_Index])))

    if regex_match is not None:
        return [regex_match.group(1), int(regex_match.group(2))]

    else:
        return ["", 0]


def f_getRoomLetter(in_room_split): return getRoomGroups(in_room_split)[0]


"""Takes a list containing the letter and then number of a room,
Returns the letter.
"""


def f_getRoomNumber(in_room_split): return getRoomGroups(in_room_split)[1]


""" TEMPLATES """

# Sets #
Blanks = (None, "")
Cycle_Range = tuple([cycle for cycle in range(1, 15)])

Spelled_Digits = {0: "zero", 1: "one", 2: "two", 3: "three", 4: "four",
                  5: "five", 6: "six", 7: "seven", 8: "eight", 9: "nine"}
Spelled_Words = {"DR.": "Doctor", "DR": "Doctor", "RECOVERY": "RE COVERY"}

# GetStrain Set
# End Sets #

# Strings
Units = "Grams"
Not_Found = "NOT FOUND"
No_Tag = "NO TAG"
Veg = "Veg"

# Formatting
Date_Format = "X%m{Separator}X%d{Separator}X%Y"
Leading_Zero_Date_Format = "%m{Separator}%d{Separator}%Y"
Batchname_Format = "{date} {State_Code} {strain_code}"
Cycle_Format = "DRYING ROOM #{Cycle}"

# Regular Expressions
Room_Pattern = re.compile(r"([A-S])-(\d{1,2})")  # COMMENT ME
Strain_Pattern = re.compile(r"(^\S.*?)(?=:|$)")  # COMMENT ME

# Column Indices
RFID_Index = 0
Weight_Index = 1
Strain_Index = 2
Room_Index = 3

# Data Ranges
Start_Row = 3
End_Row = 1000
Start_Column = RFID_Index + 1
End_Column = Room_Index + 1

""" __INIT__ """

CO_State_Code = "020"

""" EXCEL """

# Move these constants to tgs
# Sheetnames
l_Weights = "Weights"
l_Report = "Report"

# Column Ranges
RFID = "A3:A1000"
WEIGHT = "B3:B1000"
STRAIN = "C3:C1000"
ROOM = "D3:D1000"

# Sheet Ranges
Weights_Range = "$A$3:$D$1000"

# File Extensions
Extension = ".xlsx"

"""After start of steele-vs"""
Row_Limit = 400


def getSheetlist(Filename, Sheetname):
    # Takes and openpyxl workbook and sheetname
    # Returns a worksheet as a list
    workbook = openpyxl.load_workbook(Filename, read_only=True, data_only=True)
    Sheet = workbook[Sheetname]

    raw_rows = list()  # create a list at function scope
    out_filtered_rows = list()  # create a sorted list at function scope

    for row in Sheet.iter_rows(min_row=Start_Row,
                               max_row=End_Row,
                               max_col=End_Column,
                               values_only=True):
        # logger.debug("Adding raw row %s" % (str(row)))
        raw_rows.append(row)

    # Remove None values from the list
    for sublist in raw_rows:
        # Remove None values from sublist
        tmp_filtered_rows = [row for row in sublist if row is not None]

        # Check if any values are left
        if (len(tmp_filtered_rows) > 0) and ((sublist[0] is not None) and
                                             (sublist[1] is not None)):
            # logger.info("Adding filtered row: %s" % (str(sublist)))
            out_filtered_rows.append(sublist)  # append if list is left

    return out_filtered_rows


def main(harvestFile):
    """ PATH """
    # Directories
    Working_Directory = os.path.abspath(os.path.dirname(sys.argv[0]))
    # was __file__
    Output_Directory = os.path.abspath(os.path.join(Working_Directory,
                                                    "..",
                                                    "Output Files"))

    # Files
    Tmp_File = os.path.join(Working_Directory, "tmp.xlsx")
    Deletable_File = os.path.join(Working_Directory, "Safe_to_DELETE_ME.txt")
    # Log_File = os.path.join(Working_Directory, "debug.log")
    # Validated_File_Path = out_filepath[:-5] + "_Validated.xlsx"

    # File Extensions
    CSV_Extension = ".csv"
    XLSX_Extension = ".xlsx"

    """ """

    # Read Lists
    input_table = [[]]
    tags = []
    weights = []
    strains = []

    # Modified Lists
    strain_codes = []
    tag_rooms_flagged = []

    # NEW
    # def slice_per(source, step):
    #     return [source[i::step] for i in range(step)]
    # Output Variables
    csv_rows = []
    csv_rows2 = []
    csv_rows3 = []


    # Variables
    date = getDate('.')
    harvest_date = getDateForLast("Monday")  # Formerly getWednesdaysDate()
    error_move_date = getDate('/')
    if error_move_date[1] == '/':
        month = int(error_move_date[0])
        size = 1
    else:
        month = int(error_move_date[:2])
        size = 2
    month -= 2
    month = str(month)
    error_move_date = str(month) + error_move_date[size:]

    # Path
    # validated_file = input(
    #     "Drag the harvest file into this window and press enter: ")
    # import sys
    # validated_file = sys.argv[1]
    # validated_file = validated_file.strip("'\" ")
    validated_file = harvestFile
    if not os.path.exists(validated_file):
        basename = os.path.basename(validated_file)
        validated_file = os.path.join(Working_Directory, basename)
        if not os.path.exists(validated_file):
            validated_file = os.path.join(Output_Directory, basename)
            if not os.path.exists(validated_file):
                raise FileNotFoundError

    cycle = input("Enter the harvest cycle: ")

    # NEW
    harvest_csv = os.path.join(Output_Directory,
                               "{date} Harvest File.csv".format(date=date))
    harvest_csv2 = harvest_csv[:-4] + ".2.csv"
    harvest_csv3 = harvest_csv[:-4] + ".3.csv"
    error_csv = os.path.join(Output_Directory,
                             "{date} Error Move File.csv".format(date=date))


    # Get validated data
    if validated_file.endswith(CSV_Extension):
        with open(validated_file, 'r') as csv_in:
            csv_reader = csv.reader(csv_in)
            input_table = list(csv_reader)

    elif validated_file.endswith(XLSX_Extension):
        input_table = getSheetlist(validated_file,
                                   l_Weights)


    # Build Data Table
    for index, row in enumerate(input_table):
        if ("Harvest" in row[0]) or ("RFID" in row[0]):
            continue

        if row[0] in Blanks:
            continue

        tags.append(row[0])
        weights.append(row[1])
        strains.append(row[2])

        not_found = row[3] == Not_Found
        in_veg = Veg.upper() in row[3]
        if in_veg or not_found:
            room_above = f"Value Above: Flower {input_table[(index - 1)][3]}"

            error_entry = [row[0], "", "Flowering",
                           room_above, error_move_date]

            tag_rooms_flagged.append(error_entry)

    # Verify List Size
    assert len(tags) == len(weights) == len(strains)

    # Build Strain Codes
    strain_codes = getStrainCodes(strains)

    # Build CSV Table
    for index, strain in enumerate(strains):
        csv_row = []

        csv_row.append(tags[index])
        csv_row.append(weights[index])
        csv_row.append(Units)
        csv_row.append(Cycle_Format.format(Cycle=cycle))
        csv_row.append(Batchname_Format
                       .format(date=harvest_date,
                               State_Code=CO_State_Code,
                               strain_code=strain_codes[index]))
        csv_row.append(None)
        csv_row.append(harvest_date)

        csv_rows.append(csv_row)

    # NEW
    Second_File_Limit = Row_Limit * 2
    Third_File_Limit = Row_Limit * 3
    # Break CSV Table Up Every Row_Limit Rows
    if len(csv_rows) > Row_Limit:
        if len(csv_rows) > Second_File_Limit:
            if len(csv_rows) > Third_File_Limit:
                print(f"Error! {len(csv_rows)} rows is too many.")
                input("Press enter to exit: ")

            csv_rows3 = csv_rows[Second_File_Limit:Third_File_Limit]
        csv_rows2 = csv_rows[Row_Limit:Second_File_Limit]
        csv_rows = csv_rows[:Row_Limit]


    with open(harvest_csv, 'w', newline='') as harvest_out:
        csv_writer = csv.writer(harvest_out)
        csv_writer.writerows([row for row in csv_rows])

    if len(csv_rows2) > 0:
        if len(csv_rows3) > 0:
            with open(harvest_csv3, 'w', newline='') as harvest_out:
                csv_writer = csv.writer(harvest_out)
                csv_writer.writerows([row for row in csv_rows3])

        with open(harvest_csv2, 'w', newline='') as harvest_out:
            csv_writer = csv.writer(harvest_out)
            csv_writer.writerows([row for row in csv_rows2])

    if len(tag_rooms_flagged) > 0:
        with open(error_csv, 'w', newline='') as error_out:
            csv_writer = csv.writer(error_out)
            csv_writer.writerows([row for row in tag_rooms_flagged])

    print("Files saved in 'Output Files'")

    return


if __name__ == "__main__":
    main(sys.argv[1].strip("'\" "))
