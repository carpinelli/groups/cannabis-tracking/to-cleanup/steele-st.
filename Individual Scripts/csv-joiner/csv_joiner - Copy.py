#!/usr/bin/env python3

"""A script for joining multiple CSV files."""
"""TMP: Joins all CSV files into 1."""

import csv
import itertools
from pathlib import Path

from modules import helpers


def merge_lists(Lists: list) -> list:
    """Takes a list of multi-dimensional lists and merges the
    corresponding values to produce a single multi-dimensional list."""
    return [list(itertools.chain(*value))
                   for value in zip(*Lists)]


def csv_joiner() -> None:
    Cwd = Path(".").resolve()  # Get current working directory
    merged_csv = Cwd / "merged.csv"  # ADD an option to name the file
    Kilobyte = 1024  # Bytes

    # Scoped Variables
    csv_files = list()
    csv_lists = list()
    csv_header = list()

    # Get a list of all csv files in Cwd
    for file in Cwd.iterdir():
        if file.suffix.lower() == ".csv":
            csv_files.append(file)

    # Get header, if it exists
    with open(csv_files[0], 'r') as csv_in:
        if csv.Sniffer().has_header(csv_in.read(Kilobyte)):
            csv_in.seek(0)  # Reset position in file after csv_in.read()
            reader = csv.DictReader(csv_in)
            csv_header = reader.fieldnames

    # Merge the CSV files
    with open(merged_csv, 'w', newline='') as csv_out:
        csv_merger = csv.writer(csv_out)
        is_header_blank = not csv_header == []
        if is_header_blank:
            csv_merger.writerow(csv_header)
        for file in csv_files:
            with open(file, 'r') as csv_in:
                for line in csv_in:
                    if (not is_header_blank) and (csv_header in line):
                        continue  # Skip header
                    csv_merger.writerow(line)

    # # # HERE # # #
    input("Merged CSV In Directory!")
    # # # HERE # # #

    # Open each file and extract the values as lists
    # Use filename order or a specified order
    for file in csv_files:
        csv_lists.append(helpers.load_csv(file))

    # Combine the list of lists into a flat list
    joined_csv = merge_lists(csv_lists)

    helpers.to_csv(joined_csv, Cwd / "merged.csv")  # Write to csv

    return None


def main() -> None:
    csv_joiner()


if __name__ == "__main__":
    main()
