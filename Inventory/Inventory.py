#! /usr/bin/env python3

import re
import json
import csv
from pathlib import Path

import xlrd
import pandas as pd


# Setup
Phase1 = "Phase 1"
Phase2 = "Phase 2"
Phase3 = "Phase 3"
Phase4 = "Phase 4"
Inventory_Filename = "PlantsInventoryReport.xls"
Output_Filename = "Phase Counts.csv"
sheetname = "Sheet1"
header_index = 12
# USECOLS = [2, 6, 7, 10, 12]
USECOLS = [2, 6, 8, 12, 14]
Room_Pattern = re.compile(r"(\w)-(\d{1,2})")
Newroom_Pattern = re.compile(r"(\d).(\w).(\d)")

# Constants
ROOM_COL = "Location"

# Get Date
wb = xlrd.open_workbook(Inventory_Filename)  # open inventory workbook
ws = wb.sheet_by_index(0)  # get the first/only worksheet
date_cell = ws.cell_value(2, 4)  # get date range from inventory report
date_cell = date_cell.split()  # get list split by whitespace
date = date_cell[3].replace('/', '.')
# 3rd word in cell is the most recent date

# load mom rooms

moms = {}
with open("data/Moms.json") as json_file:
    moms = json.load(json_file)
    moms = moms["Moms"]

# load veg rooms

veg = {}
newveg = {}
with open("data/Veg.json") as json_file:
    veg = json.load(json_file)
with open("data/New Veg.json") as json_file:
    newveg = json.load(json_file)
for phase in veg.keys():
    veg[phase] += newveg[phase]

# load bloom rooms

bloom = {}
newbloom = {}
with open("data/Bloom.json") as json_file:
    bloom = json.load(json_file)
with open("data/New Bloom.json") as json_file:
    newbloom = json.load(json_file)
for phase in bloom.keys():
    bloom[phase] += newbloom[phase]

# load inventory from xls file

inventory = pd.read_excel(Inventory_Filename,
                          sheet_name=sheetname,
                          header=header_index,
                          usecols=USECOLS)
rooms = inventory[ROOM_COL]
rooms = rooms.value_counts()  # .values.tolist()
room_counts = {}
for room in rooms.keys():
    # strip room text
    match = Room_Pattern.search(room)
    if match is None:
        match = Newroom_Pattern.search(room)
    if match is None:
        input("Error: {room} does not match a known pattern...")

    # build dictionary
    stripped_room = match.group(0)
    room_counts[stripped_room] = rooms[room]

# total inventory

mom_count = 0
veg_count = [0, 0]
bloom_count = [0, 0, 0, 0]
for room in room_counts.keys():
    if room in moms:
        mom_count += room_counts[room]
    elif room in veg[Phase1]:
        veg_count[0] += room_counts[room]
    elif room in veg[Phase2]:
        veg_count[1] += room_counts[room]
    elif room in bloom[Phase1]:
        bloom_count[0] += room_counts[room]
    elif room in bloom[Phase2]:
        bloom_count[1] += room_counts[room]
    elif room in bloom[Phase3]:
        bloom_count[2] += room_counts[room]
    elif room in bloom[Phase4]:
        bloom_count[3] += room_counts[room]
    else:
        input(f"Error, no phase found for: {room}")

# build table
counts = []
counts.append(["Moms:", mom_count])
counts.append(["Veg Phase 1:", veg_count[0]])
counts.append(["Veg Phase 2:", veg_count[1]])
counts.append(["Bloom Phase 1:", bloom_count[0]])
counts.append(["Bloom Phase 2:", bloom_count[1]])
counts.append(["Bloom Phase 3:", bloom_count[2]])
counts.append(["Bloom Phase 4:", bloom_count[3]])

# write to csv
with open(Output_Filename, 'w', newline='') as csv_file:
    csv_writer = csv.writer(csv_file)
    csv_writer.writerows([row for row in counts])

Path(Inventory_Filename).resolve().rename(f"data/Reports/{date} "
                                          + Inventory_Filename)

print(f"Output saved as '{Output_Filename}'. Exiting...")

"""
import json
with open('data.json', 'w', encoding='utf-8') as f:
    json.dump(data, f, ensure_ascii=False, indent=4)

# room_counts.replace(regex=True, inplace=True, to_replace=Veg, value="")
# room_counts.replace(regex=True, inplace=True, to_replace=Flower, value="")
"""
